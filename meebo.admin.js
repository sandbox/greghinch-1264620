if (Drupal.jsEnabled) {	
    Drupal.behaviors.Meebo = function (context) {
        var options = $([
            '#edit-meebo-links-sharable',
            '#edit-meebo-images-sharable',
            '#edit-meebo-videos-sharable'
        ]),
        allOption = $('#edit-meebo-everything-sharable');

        allOption.change(function(e) {
            options.each(function (i, o) {
                if (e.target.checked) {
                    $(o).attr('checked', 'checked');
                } else {
                    $(o).removeAttr('checked');
                }
            });
        });
        
        if(allOption.is(':checked')) {
            options.each(function (i, o) {
                $(o).attr('checked', 'checked');
            });
        }
    }
}
